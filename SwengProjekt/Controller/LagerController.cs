﻿using SwengProjekt.Model;
using System.ComponentModel;
using System.Linq;

namespace SwengProjekt.Controller
{
    static class LagerController
    {
        public static BindingList<Güter> lager;

        static LagerController()
        {
            lager = new BindingList<Güter>();
            Initialize();
        }

        private static void Initialize()
        {
            lager.Add(new Güter("Schweinebraten", 10.90, 12));
            lager.Add(new Güter("saueres Lüngerl", 11.60, 26));
            lager.Add(new Güter("Leberkäs", 6.80, 50));
            lager.Add(new Güter("Brezn", 1.50, 303));
            lager.Add(new Güter("Obazda", 3.50, 65));
            lager.Add(new Güter("Wurstsalat", 5.50, 49));
            lager.Add(new Güter("Weißwurscht", 8.60, 45));
            lager.Add(new Güter("Weißbier", 3.50, 128));
            lager.Add(new Güter("Pils", 3.20, 166));
            lager.Add(new Güter("helles Bier", 3.40, 180));
            lager.Add(new Güter("Apfelsaftschorle", 3.00, 79));
            lager.Add(new Güter("Wasser", 2.50, 203));
        }

        public static void Insert(Güter g)
        {
            if (!lager.Contains(g))
                lager.Add(g);
            else
                lager.FirstOrDefault((lg) => lg.Name == g.Name)
                     .Count++;
        }

        public static void Remove(Güter g)
        {
            lager.Remove(g);
        }

        public static int GetBestand(Güter g)
        {
            return lager.FirstOrDefault((lg) => lg.Name == g.Name).Count;
        }

        public static void DecrementCount(Güter g)
        {
            if (lager.Contains(g))
                lager.FirstOrDefault((lg) => lg.Name == g.Name)
                     .Count--;
        }

        public static void IncrementCount(Güter g)
        {
            if (lager.Contains(g))
                lager.FirstOrDefault((lg) => lg.Name == g.Name)
                     .Count++;
        }

        public static void SetCount(Güter g, int c)
        {
            if (lager.Contains(g))
                lager.FirstOrDefault((lg) => lg.Name == g.Name)
                     .Count = c;
        }
    }
}
