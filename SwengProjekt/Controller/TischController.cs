﻿using SwengProjekt.Model;
using System.ComponentModel;

namespace SwengProjekt.Controller
{
    static class TischController
    {
        public static BindingList<Tisch> tables = new BindingList<Tisch>();

        static TischController()
        {
            tables = new BindingList<Tisch>();
            Initialize();
        }

        private static void Initialize()
        {
            tables.Add(new Tisch(1));
            tables.Add(new Tisch(2));
            tables.Add(new Tisch(3));
            tables.Add(new Tisch(4));
            tables.Add(new Tisch(5));
        }

        public static void AddGüter(Güter g)
        {
            foreach (var table in tables)
            {
                table.OldOrders.Add(new Güter(g, 0));
                table.Order.Add(new Güter(g, 0));
            }
        }

        public static void Add()
        {
            //Neuen Tisch mit fortlaufener Nummer
            tables.Add(new Tisch(tables.Count + 1));
        }

        public static void RemoveGüter(Güter g)
        {
            foreach (var table in tables)
            {
                table.OldOrders.Remove(g);
                table.Order.Remove(g);
            }
        }

        internal static void ClearAll()
        {
            foreach (var table in tables)
            {
                for (int i = 0; i < table.Order.Count; i++)
                {
                    //Lösche bereits abgeschickte bestellungen
                    table.OldOrders[i].Count = 0;
                    //Lösche noch offene Bestellungen und füge sie weider dem Lager hinzu
                    if (table.Order[i].Count != 0)
                    {
                        LagerController.SetCount(table.Order[i], LagerController.GetBestand(table.Order[i]) + table.Order[i].Count);
                        table.Order[i].Count = 0;
                    }
                    table.HasOpenOrder = false;
                }
            }
        }

        internal static void Remove()
        {
            //letzten Tisch Löschen
            if (tables.Count > 0)
                tables.RemoveAt(tables.Count - 1);
        }
    }
}
