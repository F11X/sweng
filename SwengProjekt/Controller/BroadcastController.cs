﻿using Broadcaster;
using SwengProjekt.Model;

namespace SwengProjekt.Controller
{

    class BroadcastController
    {
        public static string BESTELLUNG = "Bestellung";
        public static string RECHNUNG = "Rechnung";


        public static BroadcastContainer bc = new BroadcastContainer();
    }

    public class BroadcastMessage
    {
        public string Content { get; set; }
        public string Waiter { get; set; }
        public Tisch Table { get; set; }
    }

    public class BroadcastEvent : BroadcasterEvent<BroadcastMessage> { }
}
