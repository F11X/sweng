﻿namespace SwengProjekt
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tPMain = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gBBill = new System.Windows.Forms.GroupBox();
            this.rtbBill = new System.Windows.Forms.RichTextBox();
            this.gBCuisine = new System.Windows.Forms.GroupBox();
            this.rtbCuisine = new System.Windows.Forms.RichTextBox();
            this.gBWaiter2_tables = new System.Windows.Forms.GroupBox();
            this.panW2_tables = new System.Windows.Forms.Panel();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.gBWaiter1_tables = new System.Windows.Forms.GroupBox();
            this.panW1_tables = new System.Windows.Forms.Panel();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnBetrieb = new System.Windows.Forms.Button();
            this.tPAdmin = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gbTables = new System.Windows.Forms.GroupBox();
            this.tableLabel = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.nupSetCount = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.gbAdd = new System.Windows.Forms.GroupBox();
            this.nupCount = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl.SuspendLayout();
            this.tPMain.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gBBill.SuspendLayout();
            this.gBCuisine.SuspendLayout();
            this.gBWaiter2_tables.SuspendLayout();
            this.panW2_tables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.gBWaiter1_tables.SuspendLayout();
            this.panW1_tables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel8.SuspendLayout();
            this.tPAdmin.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.gbTables.SuspendLayout();
            this.gbEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupSetCount)).BeginInit();
            this.gbAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCount)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tPMain);
            this.tabControl.Controls.Add(this.tPAdmin);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(818, 998);
            this.tabControl.TabIndex = 0;
            // 
            // tPMain
            // 
            this.tPMain.Controls.Add(this.tableLayoutPanel1);
            this.tPMain.Controls.Add(this.panel11);
            this.tPMain.Controls.Add(this.panel10);
            this.tPMain.Controls.Add(this.panel9);
            this.tPMain.Controls.Add(this.panel8);
            this.tPMain.Location = new System.Drawing.Point(4, 22);
            this.tPMain.Margin = new System.Windows.Forms.Padding(2);
            this.tPMain.Name = "tPMain";
            this.tPMain.Padding = new System.Windows.Forms.Padding(2);
            this.tPMain.Size = new System.Drawing.Size(810, 972);
            this.tPMain.TabIndex = 0;
            this.tPMain.Text = "Übersicht";
            this.tPMain.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gBBill, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.gBCuisine, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gBWaiter2_tables, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gBWaiter1_tables, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(15, 30);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(780, 927);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // gBBill
            // 
            this.gBBill.Controls.Add(this.rtbBill);
            this.gBBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBBill.Location = new System.Drawing.Point(392, 650);
            this.gBBill.Margin = new System.Windows.Forms.Padding(2);
            this.gBBill.Name = "gBBill";
            this.gBBill.Padding = new System.Windows.Forms.Padding(2);
            this.gBBill.Size = new System.Drawing.Size(386, 275);
            this.gBBill.TabIndex = 1;
            this.gBBill.TabStop = false;
            this.gBBill.Text = "Drucker: Kasse";
            // 
            // rtbBill
            // 
            this.rtbBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbBill.Location = new System.Drawing.Point(2, 15);
            this.rtbBill.Margin = new System.Windows.Forms.Padding(2);
            this.rtbBill.Name = "rtbBill";
            this.rtbBill.ReadOnly = true;
            this.rtbBill.Size = new System.Drawing.Size(382, 258);
            this.rtbBill.TabIndex = 1;
            this.rtbBill.Text = "";
            // 
            // gBCuisine
            // 
            this.gBCuisine.Controls.Add(this.rtbCuisine);
            this.gBCuisine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBCuisine.Location = new System.Drawing.Point(2, 650);
            this.gBCuisine.Margin = new System.Windows.Forms.Padding(2);
            this.gBCuisine.Name = "gBCuisine";
            this.gBCuisine.Padding = new System.Windows.Forms.Padding(2);
            this.gBCuisine.Size = new System.Drawing.Size(386, 275);
            this.gBCuisine.TabIndex = 0;
            this.gBCuisine.TabStop = false;
            this.gBCuisine.Text = "Drucker: Ausgabe";
            // 
            // rtbCuisine
            // 
            this.rtbCuisine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCuisine.Location = new System.Drawing.Point(2, 15);
            this.rtbCuisine.Margin = new System.Windows.Forms.Padding(2);
            this.rtbCuisine.Name = "rtbCuisine";
            this.rtbCuisine.ReadOnly = true;
            this.rtbCuisine.Size = new System.Drawing.Size(382, 258);
            this.rtbCuisine.TabIndex = 0;
            this.rtbCuisine.Text = "";
            // 
            // gBWaiter2_tables
            // 
            this.gBWaiter2_tables.Controls.Add(this.panW2_tables);
            this.gBWaiter2_tables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBWaiter2_tables.Location = new System.Drawing.Point(392, 2);
            this.gBWaiter2_tables.Margin = new System.Windows.Forms.Padding(2);
            this.gBWaiter2_tables.Name = "gBWaiter2_tables";
            this.gBWaiter2_tables.Padding = new System.Windows.Forms.Padding(2);
            this.gBWaiter2_tables.Size = new System.Drawing.Size(386, 644);
            this.gBWaiter2_tables.TabIndex = 1;
            this.gBWaiter2_tables.TabStop = false;
            this.gBWaiter2_tables.Text = "Kellner 2";
            // 
            // panW2_tables
            // 
            this.panW2_tables.AutoScroll = true;
            this.panW2_tables.Controls.Add(this.elementHost2);
            this.panW2_tables.Controls.Add(this.pictureBox2);
            this.panW2_tables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panW2_tables.Location = new System.Drawing.Point(2, 15);
            this.panW2_tables.Margin = new System.Windows.Forms.Padding(2);
            this.panW2_tables.Name = "panW2_tables";
            this.panW2_tables.Size = new System.Drawing.Size(382, 627);
            this.panW2_tables.TabIndex = 1;
            // 
            // elementHost2
            // 
            this.elementHost2.AutoSize = true;
            this.elementHost2.Location = new System.Drawing.Point(49, 73);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(1, 1);
            this.elementHost2.TabIndex = 2;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = null;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(29, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(323, 618);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // gBWaiter1_tables
            // 
            this.gBWaiter1_tables.Controls.Add(this.panW1_tables);
            this.gBWaiter1_tables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBWaiter1_tables.Location = new System.Drawing.Point(2, 2);
            this.gBWaiter1_tables.Margin = new System.Windows.Forms.Padding(2);
            this.gBWaiter1_tables.Name = "gBWaiter1_tables";
            this.gBWaiter1_tables.Padding = new System.Windows.Forms.Padding(2);
            this.gBWaiter1_tables.Size = new System.Drawing.Size(386, 644);
            this.gBWaiter1_tables.TabIndex = 0;
            this.gBWaiter1_tables.TabStop = false;
            this.gBWaiter1_tables.Text = "Kellner 1";
            // 
            // panW1_tables
            // 
            this.panW1_tables.AutoScroll = true;
            this.panW1_tables.Controls.Add(this.elementHost1);
            this.panW1_tables.Controls.Add(this.pictureBox1);
            this.panW1_tables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panW1_tables.Location = new System.Drawing.Point(2, 15);
            this.panW1_tables.Margin = new System.Windows.Forms.Padding(2);
            this.panW1_tables.Name = "panW1_tables";
            this.panW1_tables.Size = new System.Drawing.Size(382, 627);
            this.panW1_tables.TabIndex = 0;
            // 
            // elementHost1
            // 
            this.elementHost1.AutoSize = true;
            this.elementHost1.Location = new System.Drawing.Point(49, 73);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1, 1);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(323, 618);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(795, 30);
            this.panel11.Margin = new System.Windows.Forms.Padding(2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(13, 927);
            this.panel11.TabIndex = 3;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(2, 30);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(13, 927);
            this.panel10.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(2, 957);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(806, 13);
            this.panel9.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnBetrieb);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(2, 2);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(806, 28);
            this.panel8.TabIndex = 0;
            // 
            // btnBetrieb
            // 
            this.btnBetrieb.Location = new System.Drawing.Point(13, 3);
            this.btnBetrieb.Name = "btnBetrieb";
            this.btnBetrieb.Size = new System.Drawing.Size(126, 23);
            this.btnBetrieb.TabIndex = 0;
            this.btnBetrieb.Text = "Betriebsmodus: Aus";
            this.btnBetrieb.UseVisualStyleBackColor = true;
            this.btnBetrieb.Click += new System.EventHandler(this.BetriebsButton_Click);
            // 
            // tPAdmin
            // 
            this.tPAdmin.Controls.Add(this.panel7);
            this.tPAdmin.Controls.Add(this.panel5);
            this.tPAdmin.Controls.Add(this.panel4);
            this.tPAdmin.Controls.Add(this.panel2);
            this.tPAdmin.Controls.Add(this.panel1);
            this.tPAdmin.Controls.Add(this.panel3);
            this.tPAdmin.Location = new System.Drawing.Point(4, 22);
            this.tPAdmin.Margin = new System.Windows.Forms.Padding(2);
            this.tPAdmin.Name = "tPAdmin";
            this.tPAdmin.Padding = new System.Windows.Forms.Padding(2);
            this.tPAdmin.Size = new System.Drawing.Size(810, 972);
            this.tPAdmin.TabIndex = 1;
            this.tPAdmin.Text = "Verfügbarkeitsverwaltung";
            this.tPAdmin.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.AutoScroll = true;
            this.panel7.Controls.Add(this.dataGridView1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(15, 34);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(780, 789);
            this.panel7.TabIndex = 7;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.price,
            this.count});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(780, 789);
            this.dataGridView1.TabIndex = 0;
            // 
            // name
            // 
            this.name.DataPropertyName = "Name";
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // price
            // 
            this.price.DataPropertyName = "Price";
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.price.DefaultCellStyle = dataGridViewCellStyle1;
            this.price.HeaderText = "Preis";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // count
            // 
            this.count.DataPropertyName = "Count";
            this.count.HeaderText = "Anzahl";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.gbTables);
            this.panel5.Controls.Add(this.gbEdit);
            this.panel5.Controls.Add(this.gbAdd);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(15, 823);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(780, 134);
            this.panel5.TabIndex = 5;
            // 
            // gbTables
            // 
            this.gbTables.Controls.Add(this.tableLabel);
            this.gbTables.Controls.Add(this.button2);
            this.gbTables.Controls.Add(this.button1);
            this.gbTables.Controls.Add(this.label8);
            this.gbTables.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbTables.Location = new System.Drawing.Point(627, 0);
            this.gbTables.Name = "gbTables";
            this.gbTables.Size = new System.Drawing.Size(148, 134);
            this.gbTables.TabIndex = 5;
            this.gbTables.TabStop = false;
            this.gbTables.Text = "Tischverwaltung";
            // 
            // tableLabel
            // 
            this.tableLabel.AutoSize = true;
            this.tableLabel.Location = new System.Drawing.Point(72, 65);
            this.tableLabel.Name = "tableLabel";
            this.tableLabel.Size = new System.Drawing.Size(13, 13);
            this.tableLabel.TabIndex = 3;
            this.tableLabel.Text = "0";
            this.tableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(67, 93);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnTableMinus);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(67, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnTablePlus);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tische:";
            // 
            // gbEdit
            // 
            this.gbEdit.Controls.Add(this.nupSetCount);
            this.gbEdit.Controls.Add(this.label7);
            this.gbEdit.Controls.Add(this.btnRemove);
            this.gbEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbEdit.Location = new System.Drawing.Point(404, 0);
            this.gbEdit.Margin = new System.Windows.Forms.Padding(2);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Padding = new System.Windows.Forms.Padding(2);
            this.gbEdit.Size = new System.Drawing.Size(223, 134);
            this.gbEdit.TabIndex = 4;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "ausgewähltes Element bearbeiten";
            // 
            // nupSetCount
            // 
            this.nupSetCount.Location = new System.Drawing.Point(120, 21);
            this.nupSetCount.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupSetCount.Name = "nupSetCount";
            this.nupSetCount.Size = new System.Drawing.Size(77, 20);
            this.nupSetCount.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Bestand setzten auf:";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(13, 53);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(184, 34);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "Element entfernen";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gbAdd
            // 
            this.gbAdd.Controls.Add(this.nupCount);
            this.gbAdd.Controls.Add(this.label5);
            this.gbAdd.Controls.Add(this.label4);
            this.gbAdd.Controls.Add(this.tbPrice);
            this.gbAdd.Controls.Add(this.tbName);
            this.gbAdd.Controls.Add(this.label3);
            this.gbAdd.Controls.Add(this.label2);
            this.gbAdd.Controls.Add(this.label6);
            this.gbAdd.Controls.Add(this.btnAdd);
            this.gbAdd.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbAdd.Location = new System.Drawing.Point(0, 0);
            this.gbAdd.Margin = new System.Windows.Forms.Padding(2);
            this.gbAdd.Name = "gbAdd";
            this.gbAdd.Padding = new System.Windows.Forms.Padding(2);
            this.gbAdd.Size = new System.Drawing.Size(404, 134);
            this.gbAdd.TabIndex = 3;
            this.gbAdd.TabStop = false;
            this.gbAdd.Text = "neues Element hinzufügen";
            // 
            // nupCount
            // 
            this.nupCount.Location = new System.Drawing.Point(176, 101);
            this.nupCount.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCount.Name = "nupCount";
            this.nupCount.Size = new System.Drawing.Size(52, 20);
            this.nupCount.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(234, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Stück";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(220, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "€";
            // 
            // tbPrice
            // 
            this.tbPrice.AccessibleName = "tbPrice";
            this.tbPrice.Location = new System.Drawing.Point(176, 61);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(38, 20);
            this.tbPrice.TabIndex = 14;
            // 
            // tbName
            // 
            this.tbName.AccessibleName = "tbName";
            this.tbName.Location = new System.Drawing.Point(176, 21);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(186, 20);
            this.tbName.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Anzahl";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Preis";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(135, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Name";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(23, 34);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 34);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Hinzufügen";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(795, 34);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(13, 923);
            this.panel4.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(2, 34);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(13, 923);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 32);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Liste der verfügbaren Speisen und Getränke:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(2, 957);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(806, 13);
            this.panel3.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 998);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(570, 512);
            this.Name = "frmMain";
            this.Text = "WirtsApp -  Bestell- und Kassensystem";
            this.tabControl.ResumeLayout(false);
            this.tPMain.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gBBill.ResumeLayout(false);
            this.gBCuisine.ResumeLayout(false);
            this.gBWaiter2_tables.ResumeLayout(false);
            this.panW2_tables.ResumeLayout(false);
            this.panW2_tables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.gBWaiter1_tables.ResumeLayout(false);
            this.panW1_tables.ResumeLayout(false);
            this.panW1_tables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.tPAdmin.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.gbTables.ResumeLayout(false);
            this.gbTables.PerformLayout();
            this.gbEdit.ResumeLayout(false);
            this.gbEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupSetCount)).EndInit();
            this.gbAdd.ResumeLayout(false);
            this.gbAdd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCount)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tPMain;
        private System.Windows.Forms.TabPage tPAdmin;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbAdd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gBBill;
        private System.Windows.Forms.GroupBox gBCuisine;
        private System.Windows.Forms.GroupBox gBWaiter2_tables;
        private System.Windows.Forms.GroupBox gBWaiter1_tables;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.RichTextBox rtbCuisine;
        private System.Windows.Forms.RichTextBox rtbBill;
        private System.Windows.Forms.Panel panW2_tables;
        private System.Windows.Forms.Panel panW1_tables;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown nupCount;
        private System.Windows.Forms.NumericUpDown nupSetCount;
        private System.Windows.Forms.Button btnBetrieb;
        private System.Windows.Forms.GroupBox gbTables;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label tableLabel;
    }
}