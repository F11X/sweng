﻿using SwengProjekt.Controller;
using SwengProjekt.Model;
using SwengProjekt.View;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SwengProjekt
{

    public partial class frmMain : Form, INotifyPropertyChanged
    {
        private bool _betrieb;
        public bool Betrieb
        {
            get
            {
                return _betrieb;
            }
            set
            {
                if (value != _betrieb)
                {
                    _betrieb = value;
                    OnPropertyChanged("Betrieb");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public frmMain()
        {
            InitializeComponent();
            InitializeBetriebsmodus();
            InitializeListBoxes();
            //init Bestands-TextBox
            nupSetCount.DataBindings.Add(new Binding("Value", dataGridView1.DataSource, "Count",
                false, DataSourceUpdateMode.OnPropertyChanged));
            //Preis-Textbox Eingabe nur Zahlen und Komma zulassen
            tbPrice.KeyPress += TbPrice_KeyPress;
            //Bei Enter im Feld Bestätigen-Button drücken
            tbName.KeyDown += EnterKeyDown;
            tbPrice.KeyDown += EnterKeyDown;
            nupCount.KeyDown += EnterKeyDown;
            //UserControls (Smartphonemocks) setzen
            elementHost1.Child = new TableUserControl("Kellner 1");
            elementHost2.Child = new TableUserControl("Kellner 2");
            elementHost1.AutoSize = true;
            elementHost2.AutoSize = true;
            var p1 = elementHost1.Parent as Panel;
            p1.AutoSize = true;
            var p2 = elementHost2.Parent as Panel;
            p2.AutoSize = true;
            //BoardCaster für Druckaufträge
            BroadcastController.bc.Event<BroadcastEvent>().Subscribe(BroadcastMethode);
            //TischVerwaltung
            tableLabel.Text = TischController.tables.Count.ToString();
        }

        private void InitializeBetriebsmodus()
        {
            Betrieb = false;
            ////Bindings
            //Smartphonemocks Sichtbarkeit
            elementHost1.DataBindings.Add(new Binding("Visible", this, "Betrieb"));
            elementHost2.DataBindings.Add(new Binding("Visible", this, "Betrieb"));
            //Farbe vom Button
            var bCol = new Binding("BackColor", this, "Betrieb");
            bCol.Format += (s, e) => { e.Value = (bool)e.Value ? Color.Green : Color.Red; };
            btnBetrieb.DataBindings.Add(bCol);
            //Enabled vom Entfernen-Button
            var bRem = new Binding("Enabled", this, "Betrieb");
            bRem.Format += (s, e) => { e.Value = !((bool)e.Value); };
            btnRemove.DataBindings.Add(bRem);
            //Enabled vom Hinzufügen-Bereich
            var bAdd = new Binding("Enabled", this, "Betrieb");
            bAdd.Format += (s, e) => { e.Value = !((bool)e.Value); };
            gbAdd.DataBindings.Add(bAdd);
            //Enable Tischverwaltung
            var bTbl = new Binding("Enabled", this, "Betrieb");
            bTbl.Format += (s, e) => { e.Value = !((bool)e.Value); };
            gbTables.DataBindings.Add(bTbl);
            //Schrift vom Betriebs-Button
            var bText = new Binding("Text", this, "Betrieb");
            bText.Format += (s, e) => { e.Value = (bool)e.Value ? "Betriebsmodus: An" : "Betriebsmodus: Aus"; };
            btnBetrieb.DataBindings.Add(bText);
        }

        protected void InitializeListBoxes()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = LagerController.lager;
        }

        private void BroadcastMethode(BroadcastMessage message)
        {
            if (message.Content == BroadcastController.BESTELLUNG)
            {
                //Drucker: Ausgabe
                PrintCuisine(message);
            }
            else if (message.Content == BroadcastController.RECHNUNG)
            {
                //Drucker: Rechnung
                PrintBill(message);
            }
        }

        private void PrintBill(BroadcastMessage message)
        {
            var table = message.Table;
            double sum = 0;
            rtbBill.Text += "==================================" + "\n";
            rtbBill.Text += table + "\t\t" + DateTime.Now + "\n";
            rtbBill.Text += "==================================" + "\n";
            foreach (var order in table.OldOrders)
            {
                if (order.Count != 0)
                {
                    rtbBill.Text += order.Name.PadRight(20) + "\t" + order.Count
                        + "\t" + string.Format("{0:c}", order.Count * order.Price) + "\n";
                    sum += order.Count * order.Price;
                }
            }
            rtbBill.Text += "\t\t\t----------" + "\n";
            rtbBill.Text += "Gesamt: " + "\t\t\t" + string.Format("{0:c}", sum) + "\n";
            rtbBill.Text += "==================================" + "\n";
            rtbBill.Text += "Es bediente Sie: " + message.Waiter + "\n";
            rtbBill.Text += "==================================" + "\n";
            rtbBill.Text += "\n";
            rtbBill.SelectionStart = rtbBill.Text.Length;
            rtbBill.ScrollToCaret();
        }

        private void PrintCuisine(BroadcastMessage message)
        {
            var table = message.Table;
            rtbCuisine.Text += "==================================" + "\n";
            rtbCuisine.Text += table + "\n";
            rtbCuisine.Text += "==================================" + "\n";
            foreach (var order in table.Order)
            {
                int newOrders = order.Count - table.OldOrders.Find(g => g.Name == order.Name).Count;
                if (order.Count != 0 && newOrders > 0)
                {
                    rtbCuisine.Text += order.Name.PadRight(20) + "\t\t" + newOrders + "\n";
                }
            }
            rtbCuisine.Text += "==================================" + "\n";
            rtbCuisine.Text += "\t\t\t(" + message.Waiter + ")\n";
            rtbCuisine.Text += "\n";
            rtbCuisine.SelectionStart = rtbCuisine.Text.Length;
            rtbCuisine.ScrollToCaret();
        }

        private void EnterKeyDown(object sender, KeyEventArgs e)
        {
            //Bei Enter im Preisfeld Bestätigen-Button drücken
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.PerformClick();
            }
        }

        private void TbPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Falls es schon ein Komma gibt -> Eingabe ignorieren
            if (e.KeyChar == ',' && tbPrice.Text.IndexOf(',') != -1)
            {
                e.Handled = true;
                return;
            }
            //Nur Zahlen und Komma zulassen
            e.Handled = !char.IsDigit(e.KeyChar)
                && !char.IsControl(e.KeyChar)
                && e.KeyChar != ',';
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            double price = 0;
            int count = 0;
            if (tbName.Text == String.Empty)
            {
                MessageBox.Show("Bitte einen Namen angeben.");
                tbName.Focus();
                return;
            }
            //Bei leerem Preisfeld Meldung ausgeben
            if (tbPrice.Text == String.Empty)
            {
                //TODO: auf 0 überprüfen
                MessageBox.Show("Bitte einen Preis angeben.");
                tbPrice.Focus();
                return;
            }
            //Textboxen konvertieren
            try
            {
                price = Convert.ToDouble(tbPrice.Text);
                if (price == 0)
                {
                    throw new ArgumentNullException("Preis ist 0");
                }
                count = Decimal.ToInt32(nupCount.Value);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ArgumentNullException))
                    MessageBox.Show("Bitte einen Preis angeben.");
                else
                    MessageBox.Show("Konvertierungsfehler: Eingabe überprüfen");
                tbPrice.Text = "";
                nupCount.Value = 0;
                return;
            }
            //Neuen Güter erzeugen
            var g = new Güter(tbName.Text, price, count);
            if (LagerController.lager.Contains(g))
            {
                MessageBox.Show("Es befindet sich bereits ein Güter mit diesem Namen in der Liste.");
            }
            else
            {
                //Güter zum Lager hinzufügen und Clients informieren
                LagerController.Insert(g);
                TischController.AddGüter(g);
                //Textboxen zurücksetzten
                tbName.Text = "";
                tbPrice.Text = "";
                nupCount.Value = 0;
            }
            tbName.Focus();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var g = dataGridView1.CurrentRow.DataBoundItem as Güter;
            //Güter aus dem Lager entfernen
            LagerController.Remove(g);
            //Clients informieren
            TischController.RemoveGüter(g);
        }

        private void BetriebsButton_Click(object sender, EventArgs e)
        {
            if (Betrieb)
            {
                DialogResult res = MessageBox.Show(
                    "Durch das Wechseln des Betriebsmodus werden alle noch "
                    + "offnen Bestellungen gelöscht. Wollen Sie wirklich fortfahren? ",
                    "Achtung", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    Betrieb = !Betrieb;
                    TischController.ClearAll();
                }
            }
            else
            {
                Betrieb = !Betrieb;
            }
        }

        private void btnTablePlus(object sender, EventArgs e)
        {
            //+
            TischController.Add();
            tableLabel.Text = TischController.tables.Count.ToString();
        }

        private void btnTableMinus(object sender, EventArgs e)
        {
            //-
            TischController.Remove();
            tableLabel.Text = TischController.tables.Count.ToString();
        }
    }
}
