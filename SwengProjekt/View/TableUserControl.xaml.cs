﻿using SwengProjekt.Controller;
using SwengProjekt.Model;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SwengProjekt.View
{
    /// <summary>
    /// Interaction logic for TableUserControl.xaml
    /// </summary>
    public partial class TableUserControl : UserControl
    {
        Tisch Table { get; set; }
        string Waiter { get; set; }

        public TableUserControl(string waiter)
        {
            InitializeComponent();
            Waiter = waiter;
            listView.ItemsSource = TischController.tables;
            orderPanel.DataContextChanged += OrderPanel_DataContextChanged;
            //Theme setzen
            ResourceDictionary dict1 = new ResourceDictionary();
            dict1.Source = new Uri("Themes/MetroDark/MetroDark.MSControls.Core.Implicit.xaml", UriKind.Relative);
            ResourceDictionary dict2 = new ResourceDictionary();
            dict2.Source = new Uri("Themes/MetroDark/MetroDark.MSControls.Toolkit.Implicit.xaml", UriKind.Relative);
            Resources.MergedDictionaries.Add(dict1);
            Resources.MergedDictionaries.Add(dict2);
        }

        private void OrderPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            Table = e.NewValue as Tisch;
            orderListView.ItemsSource = Table.Order;
            btnBill.DataContext = Table;
            
        }

        private void button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //neuen Tisch in die Datanbank einfügen
            TischController.Add();
        }

        private void TableButton_Click(object sender, RoutedEventArgs e)
        {
            //DataContext auf den ausgewählten Tisch setzten
            var b = sender as Button;
            var c = b.DataContext;
            orderPanel.DataContext = c;
            //OrderPanel sichtbar machen
            orderPanel.Visibility = System.Windows.Visibility.Visible;
            tablePanel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void BackButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //Orderpanel unsichtbar machen
            orderPanel.Visibility = System.Windows.Visibility.Hidden;
            tablePanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void Button_Click_plus(object sender, System.Windows.RoutedEventArgs e)
        {
            var s = sender as Button;
            var c = s.DataContext as Güter;
            var sp = s.Parent as StackPanel;
            if (LagerController.GetBestand(c) > 0)
            {
                c.Count++;
                LagerController.DecrementCount(c);
            }
            else
            {
                MessageBox.Show("Leider keine Lagerbestände verfügbar!", "Ausverkauft", MessageBoxButton.OK);
            }
        }

        private void Button_Click_minus(object sender, System.Windows.RoutedEventArgs e)
        {
            var s = sender as Button;
            var c = s.DataContext as Güter;
            var sp = s.Parent as StackPanel;
            var tb = sp.Children[2] as TextBox;
            if (c.Count > 0)
            {
                c.Count--;
                LagerController.IncrementCount(c);
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            //An den Drucker senden
            BroadcastController.bc.Event<BroadcastEvent>().Broadcast(new BroadcastMessage()
            {
                Content = BroadcastController.BESTELLUNG,
                Table = this.Table,
                Waiter = this.Waiter
            });
            foreach (var order in Table.Order)
            {
                int newOrders = order.Count - Table.OldOrders.Find(g => g.Name == order.Name).Count;
                if (order.Count != 0 && newOrders > 0)
                {
                    //Tisch hat ab jetzt eine offene Bestellung
                    Table.HasOpenOrder = true;
                    Table.OldOrders.Find(g => g.Name == order.Name).Count += newOrders;
                }
            }
        }

        private void BillButton_Click(object sender, RoutedEventArgs e)
        {
            //An den Drucker senden
            BroadcastController.bc.Event<BroadcastEvent>().Broadcast(new BroadcastMessage()
            {
                Content = BroadcastController.RECHNUNG,
                Table = this.Table,
                Waiter = this.Waiter
            });
            Table.HasOpenOrder = false;
            foreach (var order in Table.Order)
            {
                order.Count = 0;
                Table.OldOrders.Find(g => g.Name == order.Name).Count = 0;
            }
        }

    }
}
