﻿using SwengProjekt.Controller;
using System.Collections.Generic;
using System.ComponentModel;

namespace SwengProjekt.Model
{
    public class Tisch : INotifyPropertyChanged
    {
        public int Number { get; set; }
        public BindingList<Güter> Order { get; set; }
        public List<Güter> OldOrders { get; set; }

        private bool _ordersNotNull;
        public bool OrdersNotNull
        {
            get
            {
                return _ordersNotNull;
            }
            set
            {
                _ordersNotNull = value;
                OnPropertyChanged("OrdersNotNull");
            }
        }

        private bool _hasOpenOrder;
        public bool HasOpenOrder
        {
            get
            {
                return _hasOpenOrder;
            }
            set
            {
                _hasOpenOrder = value;
                OnPropertyChanged("HasOpenOrder");
            }
        }

        public Tisch(int nr)
        {
            Number = nr;
            OrdersNotNull = false;
            HasOpenOrder = false;
            //Vollständige leere Liste initialisieren
            OldOrders = new List<Güter>();
            Order = new BindingList<Güter>();
            foreach (Güter g in LagerController.lager)
            {
                Order.Add(new Güter(g, 0));
                OldOrders.Add(new Model.Güter(g, 0));
            }
            Order.ListChanged += Order_ListChanged;
        }

        private void Order_ListChanged(object sender, ListChangedEventArgs e)
        {
            OrdersNotNull = e.NewIndex > 0 ? true : false;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override bool Equals(object obj)
        {
            var t = obj as Tisch;
            var ret = false;
            if (t != null)
                ret = this.Number == t.Number;
            return ret;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Tisch " + Number;
        }
    }
}
