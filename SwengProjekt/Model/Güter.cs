﻿using System;
using System.ComponentModel;

namespace SwengProjekt.Model
{
    public class Güter : INotifyPropertyChanged
    {
        public String Name { get; set; }
        public double Price { get; set; }
        private int count;
        public int Count
        {
            get { return count; }
            set
            {
                if (value != count)
                {
                    count = value;
                    OnPropertyChanged("Count");
                }
            }
        }

        public Güter(string name, double price, int count)
        {
            this.Name = name;
            this.Price = price;
            this.Count = count;
        }

        public Güter(Güter güter, int cnt)
        {
            this.Name = güter.Name;
            this.Price = güter.Price;
            Count = cnt;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return this.Name;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var g = obj as Güter;
            bool ret = false;
            if (g != null)
                ret = this.Name.ToLower() == g.Name.ToLower();
            return ret;
        }
    }
}
